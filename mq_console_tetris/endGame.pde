void checkTopRow() {
  for (int i = 0; i < isFilled.length; i++) {
    //If any position off the screen isn't black
    if (isFilled[i][0] != shapeColors[0] || isFilled[i][1] != shapeColors[0]) { //[i][2] is the top row visible to the player at the moment
      //YOU DIED!
      youDied = true;
      gameOver();
    }
  }
}

void gameOver() {
  translate(0, 0);
  background(20);
  fill(255, 0, 0);
  textSize(50);
  textAlign(CENTER, CENTER);
  text("GAME OVER!", width / 2, height / 2 - height / 8);
  textSize(15);
  textAlign(CENTER, CENTER);
  text("Press Space to play again", width / 2, 2 * height / 3);
  text("Press Enter to reset", width / 2, height / 2 + height / 8);
  noLoop();
}

void startScreen() {
  translate(0, 0);
  background(20);
  fill(0);
  rectMode(CENTER);
  textAlign(CENTER, CENTER);
  imageMode(CENTER);
  image(logo, width / 2, 0.9 * (height / 3 + 200));
  boom.resize(400, 250);
  image(boom, width / 2, 0.9 * (height / 3 - 70));
  fill(255);
  textSize(22);
  text("Press Space", width / 2, (height / 3) + 200);
  textSize(16);
  text("A or LEFT to move left", width / 4, (5 * height / 6) - 20);
  text("D or RIGHT to move right", width / 4, (5 * height / 6) + 20);
  text("S or DOWN to drop", 3 *width / 4, (5 * height / 6) - 20);
  text("W or UP to rotate", 3 * width / 4, (5 * height / 6) + 20);
  rectMode(CORNER);
}



void restart() {
  //Re-initialising variables
  youDied = false;
  activeShapeRef = 1;
  currentDirection = 0;
  count = 0;
  currentPos.y = startingPos.y;
  currentPos.x = startingPos.x;
  howManyLines = 0;
  linesCleared = 0;
  score = 0;
  superSpeed = true;


  //Re-run setup
  activeShapeRef = (int)random(1, 8);
  for (int i = 0; i < isFilled.length; i++) {
    for (int j =0; j < isFilled[i].length; j++) {
      isFilled[i][j] = black;
    }
  }
  for (int i = 0; i < activeShape.length; i++) {
    activeShape[i] = new PVector(0, 0);
  }
}
