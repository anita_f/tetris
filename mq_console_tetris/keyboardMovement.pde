void keyPressed() {

  //Checking if there is a shape or screen edge to the sides
  boolean shapeToLeft = false;
  boolean shapeToRight = false;
  for (int i = 0; i < activeShape.length; i++) {
    try {
      if (isFilled[(int)(activeShape[i].x - 1)][(int)activeShape[i].y] != shapeColors[0])
        shapeToLeft = true;
    } 
    catch(Exception e) {
      shapeToLeft = true;
    }
    if (shapeToLeft) break;
  }
  for (int i = 0; i < activeShape.length; i++) {
    try {
      if (isFilled[(int)(activeShape[i].x + 1)][(int)activeShape[i].y] != shapeColors[0])
        shapeToRight = true;
    }
    catch(Exception e) {
      if (shapeToRight) break;
    }
  }
  float minX = min(min(activeShape[0].x, activeShape[1].x), min(activeShape[2].x, activeShape[3].x));
  float maxX = max(max(activeShape[0].x, activeShape[1].x), max(activeShape[2].x, activeShape[3].x));

  //Controls
  if ((keyCode == 'a' || keyCode == 'A' || keyCode == LEFT) && minX > 0 && !shapeToLeft) {
    // add another condition (&&) that checks if everything to the left is white
    currentPos.x -= 1;
  }
  if ((keyCode == 'd' || keyCode == 'D' || keyCode == RIGHT) && maxX < 9 && !shapeToRight) {
    // add another condition (&&) that checks if everything to the right is white
    currentPos.x += 1;
  }
  if ((keyCode == 'w' || keyCode == 'W' || keyCode == UP || (keyCode == ' ' && !youDied)) && rotateCheck()) {
    currentDirection++;
    currentDirection %= 4;
  }
  if ((keyCode == 'c' || keyCode == 'C') && !youDied) {
    activeShapeRef = 7;
  }
  if (youDied && keyCode==' ') {
    gameRun = true;
    restart();
    loop();
  }
  if (youDied && (keyCode == RETURN || keyCode == ENTER)) {
    gameRun = false;
    startScreen();
    loop();
  }
}

//Soft Drop
void softDrop() {
  if (superSpeed == true) {
    if (keyPressed && (key == 's' || key == 'S' || keyCode == DOWN)) {
      speed = (int)5/difficulty;
    } else {
      speed = (int)60/difficulty;
    }
  }
}

void keyReleased() {
  if (key == 's' || key == 'S' || keyCode == DOWN) {
    superSpeed = true;
  }
}
