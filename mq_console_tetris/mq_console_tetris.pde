
//Shape colours
int alpha = 150;
color black = color(0);
color blue = color(128, 206, 255, alpha);
color red = color(255, 105, 97, alpha);
color orange = color(255, 179, 71, alpha);
color green = color(207, 240, 204, alpha);
color pink = color(242, 184, 216, alpha);
color purple = color(177, 156, 217, alpha);
color yellow = color(253, 253, 150, alpha);
color[] shapeColors = {black, blue, red, orange, green, pink, purple, yellow};

//Shape and reference position
PVector startingPos = new PVector(4, 0);
PVector currentPos = new PVector(startingPos.x, startingPos.y);
PVector[] activeShape = new PVector[4];

//Global variables
int activeShapeRef = 1;
int currentDirection = 0;
int count = 0;
int scrheight;
int score = 0;
int linesCleared = 0;
int howManyLines = 0;
int volume = 10;
int difficulty = 1;
int boomCount;
int grid = 50;
int speed = 60;

boolean gameRun = false;
boolean superSpeed = true;
boolean youDied = true;
boolean changeCol = false;

//Gameboard
color[][] isFilled; // [x][y] [cols][rows]

// Sound
import processing.sound.*;
SoundFile Boom;
SoundFile tetrisMusic;

//Slider
import controlP5.*;
ControlP5 volControl;
ControlP5 diffControl;

//Image
PImage logo;
PImage boom;


void setup() {
  size(500, 800);
  
  //Initialize gameboard
  scrheight = height/50;
  isFilled = new color[10][scrheight + 4];
  
  //Pick new shape
  activeShapeRef = (int)random(1, 8);
  
  //Fill gameboard with black tiles
  for (int i = 0; i < isFilled.length; i++) {
    for (int j =0; j < isFilled[i].length; j++) {
      isFilled[i][j] = black;
    }
  }
  
  //Draw shape
  strokeWeight(5);
  for (int i = 0; i < activeShape.length; i++) {
    activeShape[i] = new PVector(0, 0);
  }
  //Sound
  Boom = new SoundFile(this, "../../tetris/data/Boom.wav");
  tetrisMusic = new SoundFile(this, "../../tetris/data/tetrisMusic.wav");
  tetrisMusic.loop();
  
  //Slider
  volControl = new ControlP5(this);
  diffControl = new ControlP5(this);
  diffControl.addSlider("difficulty").setPosition(width/14, height/40).setRange(1, 5);
  volControl.addSlider("volume").setPosition(2*width/3, (height/40)).setRange(0, 100);

  //Image
  logo = loadImage("../../tetris/data/logo.png");
  boom = loadImage("../../tetris/data/boomstart.png");
  
}

void draw() {
  
  //Game volume
  tetrisMusic.amp((float)volume/500);
  
  //Do this if game is active/alive
  if (gameRun) {
    background(0);
    
    //Moves the gamescreen so that the top two rows are hidden off screen
    translate(0, -100);
    
    //Draw gameboard with placed pieces
    for (int i = 0; i < isFilled.length; i++) {
      for (int j =0; j < isFilled[i].length; j++) {
        stroke(0);
        fill(isFilled[i][j]);
        rect(i*grid, j*grid, grid, grid, 7);
      }
    }
    
    //Change colour for new shape
    if (changeCol == true) {
      activeShapeRef = (int)random(1, 8);
      changeCol = false;
    }
    
    //Draw shape
    for (int i = 0; i < activeShape.length; i++) {
      fill(255);
      rect(activeShape[i].x * grid, (activeShape[i].y) * grid, grid, grid, 7);
    }
    for (int i = 0; i < activeShape.length; i++) {
      fill(shapeColors[activeShapeRef]);
      rect(activeShape[i].x * grid, (activeShape[i].y) * grid, grid, grid, 7);
    }
    assignShape ();
    
    //Game logic
    moveShape();
    softDrop();
    BOOMTetrisForJeff();
    checkTopRow();
    score();
    displayScore();
    
    //BOOM happens here!
    if (frameCount  > 30) {
      if (frameCount < boomCount + 25) {
        boom.resize(250, 156);
        image(boom, random(125, width - 125), random(200, height));
      }
    }
  } else {
    
    //Display the start screen
    startScreen();
  }
}
