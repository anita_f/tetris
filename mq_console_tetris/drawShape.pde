//Switch to call the active shape
void assignShape () {
  fill(shapeColors[activeShapeRef]);
  switch (activeShapeRef) {
  case 1:
    LSHAPE_B();
    break;
  case 2:
    LSHAPE_R();
    break;
  case 3:
    BOX_O();
    break;
  case 4:
    ZSHAPE_G();
    break;
  case 5:
    ZSHAPE_P();
    break;
  case 6:
    TSHAPE();
    break;
  case 7:
    LONG();
    break;
  default:
  }
}

//Positions of the four tiles of each shape
void LONG() {
  if (currentDirection == 0 || currentDirection == 2) {
    activeShape[0].x = currentPos.x;
    activeShape[0].y = currentPos.y;
    activeShape[1].x = currentPos.x+1;
    activeShape[1].y = currentPos.y;
    activeShape[2].x = currentPos.x+2;
    activeShape[2].y = currentPos.y;
    activeShape[3].x = currentPos.x-1;
    activeShape[3].y = currentPos.y;
  } else if (currentDirection == 1 || currentDirection == 3) {
    activeShape[0].x = currentPos.x;
    activeShape[0].y = currentPos.y;
    activeShape[1].x = currentPos.x;
    activeShape[1].y = currentPos.y+1;
    activeShape[2].x = currentPos.x;
    activeShape[2].y = currentPos.y+2;
    activeShape[3].x = currentPos.x;
    activeShape[3].y = currentPos.y-1;
  }
}

void LSHAPE_R() {
  if (currentDirection == 0) {
    activeShape[0].x = currentPos.x;
    activeShape[0].y = currentPos.y;
    activeShape[1].x = currentPos.x+1;
    activeShape[1].y = currentPos.y;
    activeShape[2].x = currentPos.x-1;
    activeShape[2].y = currentPos.y;
    activeShape[3].x = currentPos.x+1;
    activeShape[3].y = currentPos.y-1;
  } else if (currentDirection == 1) {
    activeShape[0].x = currentPos.x;
    activeShape[0].y = currentPos.y;
    activeShape[1].x = currentPos.x;
    activeShape[1].y = currentPos.y+1;
    activeShape[2].x = currentPos.x;
    activeShape[2].y = currentPos.y-1;
    activeShape[3].x = currentPos.x-1;
    activeShape[3].y = currentPos.y-1;
  } else if (currentDirection == 2) {
    activeShape[0].x = currentPos.x;
    activeShape[0].y = currentPos.y;
    activeShape[1].x = currentPos.x+1;
    activeShape[1].y = currentPos.y;
    activeShape[2].x = currentPos.x-1;
    activeShape[2].y = currentPos.y;
    activeShape[3].x = currentPos.x-1;
    activeShape[3].y = currentPos.y+1;
  } else if (currentDirection == 3) {
    activeShape[0].x = currentPos.x;
    activeShape[0].y = currentPos.y;
    activeShape[1].x = currentPos.x;
    activeShape[1].y = currentPos.y+1;
    activeShape[2].x = currentPos.x;
    activeShape[2].y = currentPos.y-1;
    activeShape[3].x = currentPos.x+1;
    activeShape[3].y = currentPos.y+1;
  }
}
void LSHAPE_B() {
  if (currentDirection == 0) {
    activeShape[0].x = currentPos.x;
    activeShape[0].y = currentPos.y;
    activeShape[1].x = currentPos.x+1;
    activeShape[1].y = currentPos.y;
    activeShape[2].x = currentPos.x-1;
    activeShape[2].y = currentPos.y;
    activeShape[3].x = currentPos.x+1;
    activeShape[3].y = currentPos.y+1;
  } else if (currentDirection == 1) {
    activeShape[0].x = currentPos.x;
    activeShape[0].y = currentPos.y;
    activeShape[1].x = currentPos.x;
    activeShape[1].y = currentPos.y+1;
    activeShape[2].x = currentPos.x;
    activeShape[2].y = currentPos.y-1;
    activeShape[3].x = currentPos.x+1;
    activeShape[3].y = currentPos.y-1;
  } else if (currentDirection == 2) {
    activeShape[0].x = currentPos.x;
    activeShape[0].y = currentPos.y;
    activeShape[1].x = currentPos.x+1;
    activeShape[1].y = currentPos.y;
    activeShape[2].x = currentPos.x-1;
    activeShape[2].y = currentPos.y;
    activeShape[3].x = currentPos.x-1;
    activeShape[3].y = currentPos.y-1;
  } else if (currentDirection == 3) {
    activeShape[0].x = currentPos.x;
    activeShape[0].y = currentPos.y;
    activeShape[1].x = currentPos.x;
    activeShape[1].y = currentPos.y+1;
    activeShape[2].x = currentPos.x;
    activeShape[2].y = currentPos.y-1;
    activeShape[3].x = currentPos.x-1;
    activeShape[3].y = currentPos.y+1;
  }
}

void TSHAPE() {
  if (currentDirection == 0) {
    activeShape[0].x = currentPos.x;
    activeShape[0].y = currentPos.y;
    activeShape[1].x = currentPos.x+1;
    activeShape[1].y = currentPos.y;
    activeShape[2].x = currentPos.x-1;
    activeShape[2].y = currentPos.y;
    activeShape[3].x = currentPos.x;
    activeShape[3].y = currentPos.y-1;
  } else if (currentDirection == 1) {
    activeShape[0].x = currentPos.x;
    activeShape[0].y = currentPos.y;
    activeShape[1].x = currentPos.x;
    activeShape[1].y = currentPos.y+1;
    activeShape[2].x = currentPos.x;
    activeShape[2].y = currentPos.y-1;
    activeShape[3].x = currentPos.x-1;
    activeShape[3].y = currentPos.y;
  } else if (currentDirection == 2) {
    activeShape[0].x = currentPos.x;
    activeShape[0].y = currentPos.y;
    activeShape[1].x = currentPos.x+1;
    activeShape[1].y = currentPos.y;
    activeShape[2].x = currentPos.x-1;
    activeShape[2].y = currentPos.y;
    activeShape[3].x = currentPos.x;
    activeShape[3].y = currentPos.y+1;
  } else if (currentDirection == 3) {
    activeShape[0].x = currentPos.x;
    activeShape[0].y = currentPos.y;
    activeShape[1].x = currentPos.x;
    activeShape[1].y = currentPos.y+1;
    activeShape[2].x = currentPos.x;
    activeShape[2].y = currentPos.y-1;
    activeShape[3].x = currentPos.x+1;
    activeShape[3].y = currentPos.y;
  }
}

void ZSHAPE_P() {
  if (currentDirection == 0 || currentDirection == 2) {
    activeShape[0].x = currentPos.x;
    activeShape[0].y = currentPos.y;
    activeShape[1].x = currentPos.x+1;
    activeShape[1].y = currentPos.y;
    activeShape[2].x = currentPos.x;
    activeShape[2].y = currentPos.y-1;
    activeShape[3].x = currentPos.x-1;
    activeShape[3].y = currentPos.y-1;
  } else if (currentDirection == 1 || currentDirection == 3) {
    activeShape[0].x = currentPos.x;
    activeShape[0].y = currentPos.y;
    activeShape[1].x = currentPos.x+1;
    activeShape[1].y = currentPos.y;
    activeShape[2].x = currentPos.x+1;
    activeShape[2].y = currentPos.y-1;
    activeShape[3].x = currentPos.x;
    activeShape[3].y = currentPos.y+1;
  }
}

void ZSHAPE_G() {
  if (currentDirection == 0 || currentDirection == 2) {
    activeShape[0].x = currentPos.x;
    activeShape[0].y = currentPos.y;
    activeShape[1].x = currentPos.x;
    activeShape[1].y = currentPos.y-1;
    activeShape[2].x = currentPos.x+1;
    activeShape[2].y = currentPos.y-1;
    activeShape[3].x = currentPos.x-1;
    activeShape[3].y = currentPos.y;
  } else if (currentDirection == 1 || currentDirection == 3) {
    activeShape[0].x = currentPos.x;
    activeShape[0].y = currentPos.y;
    activeShape[1].x = currentPos.x+1;
    activeShape[1].y = currentPos.y;
    activeShape[2].x = currentPos.x+1;
    activeShape[2].y = currentPos.y+1;
    activeShape[3].x = currentPos.x;
    activeShape[3].y = currentPos.y-1;
  }
}

void BOX_O() {
  if (currentDirection == 0 || currentDirection == 1 || currentDirection == 2 || currentDirection == 3) {
    activeShape[0].x = currentPos.x;
    activeShape[0].y = currentPos.y;
    activeShape[1].x = currentPos.x+1;
    activeShape[1].y = currentPos.y;
    activeShape[2].x = currentPos.x;
    activeShape[2].y = currentPos.y+1;
    activeShape[3].x = currentPos.x+1;
    activeShape[3].y = currentPos.y+1;
  }
}
