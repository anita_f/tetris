void moveShape() {
  if (frameCount % speed == 0 && !youDied) {
    //If the piece isn't colliding with anything
    if (!isCollision()) {
      //Move piece down one tile
      currentPos.y++;
    } else {
      //Colour that location on the gameboard with the active shapes colour
      for (int i = 0; i < activeShape.length; i++) {
        isFilled[(int)activeShape[i].x][(int)activeShape[i].y] = shapeColors[activeShapeRef];

        //Move the reference position to it's intial value at the top and reset direction
        currentDirection = 0;
        currentPos.y = startingPos.y;
        currentPos.x = startingPos.x;
        changeCol = true;
        //Soft drop logic to stop soft drop upon collision
        if (keyPressed && (key == 's' || key == 'S' || keyCode == DOWN)) {
          superSpeed = false;
          speed = (int)60/difficulty;
        }
      }
    }
  }
}

//Checks for collisions
boolean isCollision() {
  for (int i = 0; i < activeShape.length; i++) {    
    if ((int)activeShape[i].y - 1 >=scrheight || isFilled[(int)activeShape[i].x][(int)activeShape[i].y + 1] != shapeColors[0]) {
      return true;
    }
  }
  return false;
}

//Checks if it's safe to rotate a piece
boolean rotateCheck() {
  //Increments Direction
  if (currentDirection < 3) {
    currentDirection++;
  } else {
    currentDirection = 0;
  }
  //Creates a ghost shape for testing
  assignShape ();
  PVector[] checkShape = new PVector[4];
  for (int i = 0; i < activeShape.length; i++) {
    int xPos = (int)activeShape[i].x;
    int yPos = (int)activeShape[i].y;
    checkShape[i] = new PVector(xPos, yPos);
  }

  //Puts the direction of the active shape back as it was
  if (currentDirection > 0) {
    currentDirection--;
  } else {
    currentDirection = 3;
  }
  assignShape ();

  //The check itself
  for (int i = 0; i < checkShape.length; i++) {
    if ((int)checkShape[i].x < 0 || (int)checkShape[i].x + 1 > 10 || (int)checkShape[i].y < 0 || (int)checkShape[i].y + 1 > scrheight + 2 || isFilled[(int)checkShape[i].x][(int)checkShape[i].y] != shapeColors[0]) {
      return false;
    }
  }
  return true;
}
