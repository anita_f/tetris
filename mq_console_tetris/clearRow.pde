void BOOMTetrisForJeff () {
  for (int i = 0; i < isFilled[0].length; i++) {
    //Check if each row is full
    if (rowCheck(i)) {
      //If so clear that row
      clearRow(i);
    }
  }
}

boolean rowCheck(int index) {
  for (int i = 0; i < isFilled.length; i++) {
    if (isFilled[i][index] == shapeColors[0]) {
      return false;
    }
  }
  return true;
}

void clearRow(int index) {
  for (int j = index; j >= 1; j--) {
    for (int i = 0; i < isFilled.length; i++) {
      isFilled[i][j] = isFilled[i][j-1];
    }
  }
  //Increments Lines and Score when a row is cleared
  howManyLines++;
  linesCleared++;
}

void score() {
  if (howManyLines == 1) {
    score += difficulty*40;
  }
  if (howManyLines == 2) {
    score += difficulty*100;
  }
  if (howManyLines == 3) {
    score += difficulty*300;
  }
  if (howManyLines == 4) {
    score += difficulty*1200;
    //Sound
    Boom.play();
    //Boom Animation Delay Logic
    boomCount = frameCount;
  }
  howManyLines = 0;
}

//Draws the score and lines cleared in top left
void displayScore() {
  fill(255);
  textAlign(LEFT);
  textSize(15);
  text("Lines: "+ linesCleared, width/50, 125);
  text("Score: " + score, width/50, 140);
}
